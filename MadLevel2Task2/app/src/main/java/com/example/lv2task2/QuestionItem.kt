package com.example.lv2task2

data class QuestionItem(var quesh: String, var answer: Boolean) {
    companion object {
        val QUESTIONS = arrayOf(
            "Born to Be Alive is a song produced by The Village People",
            "Dolphins and whales are fish",
            "The fifth A on a piano has a standard tuning of 440 hertz",
            "Dogs are objectively the best kind of animal that roam this planet",
            "Smurfs are green",
            "I can gain superhuman strength by staring into the sun for 15 minutes",
            "The Dead Sea is one of the world's saltiest bodies of water, with 33.7% salinity",
            "This app is brought to you by Ruben de Graaf",
            "Ruben de Graaf definitely deserves a passing grade for the Mobile Application development course"

        )
        val ANSWERS = arrayOf(
            true,
            false,
            true,
            true,
            false,
            false,
            true,
            true,
            true
        )
    }
}