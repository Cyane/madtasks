package com.example.lv2task2


import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.material.snackbar.Snackbar


private const val DEBUG_TAG = "Gestures"

class MainActivity : AppCompatActivity()
{

    private val questions = arrayListOf<QuestionItem>()
    private val questionAdapter = QuestionAdapter(questions)

    private fun createItemTouchHelper(): ItemTouchHelper {
        val itemTouchHelperCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition

                if(questions[position].answer && direction == ItemTouchHelper.RIGHT ||
                    !questions[position].answer && direction == ItemTouchHelper.LEFT)
                {
                    questions.removeAt(position)
                    snackbarAnswer(true)
                }
                else{
                    snackbarAnswer(false)
                }
                //Reposition the viewholders
                questionAdapter.notifyDataSetChanged()
            }
        }
        return ItemTouchHelper(itemTouchHelperCallback)
    }

    private fun snackbarAnswer(answerCorrect: Boolean) {
        if(answerCorrect){
            Snackbar.make(root_layout,getString(R.string.correct), Snackbar.LENGTH_SHORT).show()
        }
        else{
            Snackbar.make(root_layout,getString(R.string.incorrect), Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    fun initViews(){
        rvQuizList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL ,false)
        rvQuizList.adapter = questionAdapter

        createItemTouchHelper().attachToRecyclerView(rvQuizList)

        for (i in QuestionItem.QUESTIONS.indices) {
            questions.add(QuestionItem(QuestionItem.QUESTIONS[i],QuestionItem.ANSWERS[i]))
        }
        questionAdapter.notifyDataSetChanged()
    }


}