package com.example.madlevel3task2

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResultListener
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_portal.*


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */

val portals = arrayListOf<Portal>()

class PortalFragment : Fragment() {

    private val portalAdapter = PortalAdapter(portals,{ portalItem : Portal ->
        openURL(portalItem.portalURL)
     })


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_portal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setFragmentResultListener(AddPortalFragment.REQ_PORTAL_KEY) { key, bundle ->
            bundle.getParcelable<Portal>(AddPortalFragment.BUNDLE_PORTAL_KEY)?.let { portals.add(it) }
            portalAdapter.notifyDataSetChanged()
        }
        rvPortals.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
        rvPortals.adapter = portalAdapter
    }

    private fun openURL(URL : String){
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        this.context?.let { customTabsIntent.launchUrl(it, Uri.parse(URL)) };
    }

}

