package com.example.madlevel3task2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_add_portal.*

//This is the solution, yessss
//https://stackoverflow.com/questions/50754523/how-to-get-a-result-from-fragment-using-navigation-architecture-component

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddPortalFragment : Fragment() {

    companion object {
        const val REQ_PORTAL_KEY = "req_portal"
        const val BUNDLE_PORTAL_KEY = "bundle_portal"
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_portal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.btnAddPortal).setOnClickListener {
            onAddPortal()
        }
    }

    private fun onAddPortal()
    {
        val portal = Portal(etPortalName.text.toString(), etPortalURL.text.toString())

        if(portal.portalName.isNotBlank() && portal.portalURL.isNotBlank())
        {
            val bundle = Bundle()
            bundle.putParcelable(BUNDLE_PORTAL_KEY, portal)  //Key, value

            setFragmentResult(REQ_PORTAL_KEY, bundle)

            findNavController().navigate(
                R.id.action_addPortalFragment_to_portalFragment
            )
        } else {
            Toast.makeText(
                activity,
                R.string.invalid_portal, Toast.LENGTH_SHORT
            ).show()
        }
    }
}