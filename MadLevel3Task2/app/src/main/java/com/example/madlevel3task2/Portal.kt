package com.example.madlevel3task2

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Portal (val portalName : String, val portalURL : String) : Parcelable


