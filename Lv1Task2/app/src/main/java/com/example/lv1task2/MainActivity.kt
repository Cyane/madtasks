package com.example.lv1task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


val answerColumn = listOf("T", "F", "F", "F")
val acceptedLetters = "tf"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submitBtn.setOnClickListener{
            checkAnswers()
        }
    }

    fun checkAnswers()
    {
        var correctAnswers = 0
        if(answer.text.toString().equals( answerColumn[0], ignoreCase = true )) {
            correctAnswers ++
        }
        if(answer1.text.toString().equals( answerColumn[1], ignoreCase = true )) {
            correctAnswers ++
        }
        if(answer2.text.toString().equals( answerColumn[2], ignoreCase = true )) {
            correctAnswers ++
        }
        if(answer3.text.toString().equals( answerColumn[3], ignoreCase = true )) {
            correctAnswers ++
        }

        Toast.makeText(this, getString(R.string.correct_answers, correctAnswers), Toast.LENGTH_SHORT).show()
    }
}

