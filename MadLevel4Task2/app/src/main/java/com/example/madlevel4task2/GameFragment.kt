package com.example.madlevel4task2

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.madlevel4task2.databinding.FragmentGameBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class GameFragment : Fragment() {

    private var _binding: FragmentGameBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var matchRepository: MatchRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentGameBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        matchRepository = MatchRepository(requireContext())

        binding.btnRock.setOnClickListener {
            playRPS(0)
        }
        binding.btnPaper.setOnClickListener {
            playRPS(1)
        }
        binding.btnScissors.setOnClickListener {
            playRPS(2)
        }

        //grabs last match from database to show
        mainScope.launch {
            withContext(Dispatchers.IO) {
               // val match: Match? = matchRepository.getLastMatch()
                //if(match != null){
                    updateLastMatch(matchRepository.getLastMatch())
                //}
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun playRPS(move: Int) {
        var cpuMove = (0..2).random()
        mainScope.launch {
            val match = Match(
                date = ,
                win = decideWinner(move, cpuMove).toShort(),
                movePlr = move.toShort(),
                moveCpu = cpuMove.toShort()
            )
            withContext(Dispatchers.IO) {
                matchRepository.insertMatch(match)
            }
            updateLastMatch(match)
        }
    }

    fun updateLastMatch(match:Match?){

        if(match != null){
            when(match.moveCpu.toInt()){
                0-> binding.imgCpuMove.setImageResource(R.drawable.rock)
                1-> binding.imgCpuMove.setImageResource(R.drawable.paper)
                2-> binding.imgCpuMove.setImageResource(R.drawable.scissors)
            }
            when(match.movePlr.toInt()){
                0-> binding.imgPlayerMove.setImageResource(R.drawable.rock)
                1-> binding.imgPlayerMove.setImageResource(R.drawable.paper)
                2-> binding.imgPlayerMove.setImageResource(R.drawable.scissors)
            }
            when(match.win.toInt()){
                0 -> binding.tvWinner.setText(R.string.draw_text)
                1 -> binding.tvWinner.setText(R.string.win_text)
                2 -> binding.tvWinner.setText(R.string.loss_text)
                else -> binding.tvWinner.setText(R.string.error_text)
            }
        }
        mainScope.launch {
                binding.tvStatistics.setText("Draws: " + matchRepository.countDraws().toString() + " Wins: "  + matchRepository.countWins().toString() + " Losses: " + matchRepository.countLosses().toString())
        }
    }

    fun decideWinner(m1: Int, m2: Int): Int {//0 = draw. 1 = player win. 2 = cpu win
        var result: Int = -1
        when (m1) {
            0 -> when (m2) {
                0 -> result = 0
                1 -> result = 2
                2 -> result = 1
            }
            1 -> when (m2) {
                0 -> result = 1
                1 -> result = 0
                2 -> result = 2
            }
            2 -> when (m2) {
                0 -> result = 2
                1 -> result = 1
                2 -> result = 0
            }
        }
        return result
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_history -> {
                findNavController().navigate(R.id.action_gameFragment_to_historyFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
