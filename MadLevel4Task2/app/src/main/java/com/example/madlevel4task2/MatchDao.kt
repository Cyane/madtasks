package com.example.madlevel4task2

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MatchDao {

    @Query("SELECT * FROM matchResultTable")
    suspend fun getAllMatches(): List<Match>

    @Query("SELECT * FROM matchResultTable ORDER BY id DESC LIMIT 1")
    suspend fun getLastMatch(): Match

    @Query( "SELECT COUNT(result) FROM matchResultTable WHERE result = 0")
    suspend fun countDraws(): Int

    @Query( "SELECT COUNT(result) FROM matchResultTable WHERE result = 1")
    suspend fun countWins(): Int

    @Query( "SELECT COUNT(result) FROM matchResultTable WHERE result = 2")
    suspend fun countLosses(): Int

    @Insert
    suspend fun insertMatch(match: Match)

    @Query("DELETE FROM matchResultTable")
    suspend fun deleteAllMatches()

}
