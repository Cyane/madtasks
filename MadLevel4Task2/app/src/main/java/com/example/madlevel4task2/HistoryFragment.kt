package com.example.madlevel4task2

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.madlevel4task2.databinding.FragmentHistoryBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class HistoryFragment : Fragment() {

    private var _binding: FragmentHistoryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var matchRepository: MatchRepository
    private val mainScope = CoroutineScope(Dispatchers.Main)

    private val matches = arrayListOf<Match>()
    private val matchHistoryAdapter = MatchHistoryAdapter(matches)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        matchRepository = MatchRepository(requireContext())
        getMatchesFromDatabase()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private fun initViews() {
        // Initialize the recycler view with a linear layout manager, adapter
        binding.rvMatchHistory.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        binding.rvMatchHistory.addItemDecoration(dividerItemDecoration)

        binding.rvMatchHistory.adapter = matchHistoryAdapter
    }

    private fun getMatchesFromDatabase() {
        mainScope.launch {
            val matchHistory = withContext(Dispatchers.IO) {
                matchRepository.getAllMatches()
            }
            matches.clear()
            matches.addAll(matchHistory)
            matchHistoryAdapter.notifyDataSetChanged()
        }
    }

    //CUSTOM APP BAR is launched here
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_history, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_delete ->{
                mainScope.launch {
                    withContext(Dispatchers.IO) {
                        matchRepository.deleteAllMatches()
                        getMatchesFromDatabase()
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}