package com.example.madlevel4task2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.madlevel4task2.databinding.ItemMatchBinding

class MatchHistoryAdapter (private val matches: List<Match>):
    RecyclerView.Adapter<MatchHistoryAdapter.ViewHolder>()
{

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        private val binding = ItemMatchBinding.bind(itemView)

        fun databind(match: Match) {
            binding.tvResult.setText(winText(match.win.toInt()))
            binding.ivCpu.setImageResource(rpsImg(match.moveCpu.toInt()))
            binding.ivPlayer.setImageResource(rpsImg(match.movePlr.toInt()))
        }
    }

    /**
     * Creates and returns a ViewHolder object, inflating a standard layout called simple_list_item_1.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_match, parent, false)
        )
    }

    /**
     * Returns the size of the list
     */
    override fun getItemCount(): Int {
        return matches.size
    }

    fun winText(input: Int): Int{
         return when (input) {
            0 -> R.string.draw_text
            1 -> R.string.win_text
            2 -> R.string.loss_text
            else -> R.string.error_text
        }
    }
    fun rpsImg(input: Int): Int{
        return when (input) {
            0 -> R.drawable.rock
            1 -> R.drawable.paper
            2 -> R.drawable.scissors
            else -> R.drawable.ic_delete_white_24dp
        }
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.databind(matches[position])
    }


}
