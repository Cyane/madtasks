package com.example.madlevel4task2

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "matchResultTable")
data class Match(

    @ColumnInfo(name = "date")
    var date:Date,

    @ColumnInfo(name = "result")//0 = draw, 1 = player win, 2 = cpu win
    var win: Short,

    @ColumnInfo(name = "cpuChoice")// 0R, 1P, 2S
    var moveCpu: Short,

    @ColumnInfo(name = "plrChoice")// 0R, 1P, 2S
    var movePlr: Short,

    /*//todo: figure out date objects and conversion in room databases
    @ColumnInfo(name = "date")
    var date: Date, //?
    */

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null

)