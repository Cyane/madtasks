package com.example.madlevel4task2

import android.content.Context


class MatchRepository(context: Context) {

    private val matchDao: MatchDao

    init {
        val database = MatchHistoryRoomDatabase.getDatabase(context)
        matchDao = database!!.matchDao()
    }

    suspend fun getAllMatches(): List<Match> {
        return matchDao.getAllMatches()
    }
    suspend fun getLastMatch(): Match {
        return matchDao.getLastMatch()
    }

    suspend fun countDraws(): Int {
        return matchDao.countDraws()
    }
    suspend fun countWins(): Int {
        return matchDao.countWins()
    }
    suspend fun countLosses(): Int {
        return matchDao.countLosses()
    }

    suspend fun insertMatch(match: Match) {
        matchDao.insertMatch(match)
    }

    suspend fun deleteAllMatches() {
        matchDao.deleteAllMatches()
    }
}